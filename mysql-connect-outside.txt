
mysql -u root -p -h 127.0.0.1

// check what permission the root user has
select user,host from mysql.user where user='root';

// give privileges to connect from everywhere (this needs to be changed to only your ip
// grant all privileges on *.* to 'root'@'%' with grant option;

GRANT ALL ON *.* to root@'192.168.34.1' IDENTIFIED BY 'mysql_root_password';

FLUSH PRIVILEGES;