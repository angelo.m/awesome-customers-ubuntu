#
# Cookbook Name:: awesome_customers_ubuntu
# Recipe:: database
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

# Configure the MySQL client
mysql_client 'default' do
    action :create
end

# Configure the MySQL service
mysql_service 'default' do
    port '3306'
    version '5.5'
    initial_root_password node['awesome_customers_ubuntu']['database']['root_password']
    bind_address '0.0.0.0'
    action [:create, :start]
end

# Install the mysql2 Ruby gem
mysql2_chef_gem 'default' do
    action :install
end


# Create connection info as an external ruby hash
connection_info = {
    :host     => node['awesome_customers_ubuntu']['database']['host'],
    :username => node['awesome_customers_ubuntu']['database']['root_username'],
    :password => node['awesome_customers_ubuntu']['database']['root_password']
}

# Create the database instance
mysql_database node['awesome_customers_ubuntu']['database']['dbname'] do
    connection connection_info
    action :create
end

# Add a database user
# this user can connect from the outside
mysql_database_user node['awesome_customers_ubuntu']['database']['admin_username'] do
    connection connection_info
    password node['awesome_customers_ubuntu']['database']['admin_password']
    database_name node['awesome_customers_ubuntu']['database']['dbname']
    action [:create, :grant]
    host '%'
    privileges [:select,:update,:insert,:create]
end

# Create a path to the SQL file in the Chef cache.
create_tables_script_path = File.join(Chef::Config[:file_cache_path], 'create-tables.sql')

# Write the SQL script to the filesystem
cookbook_file create_tables_script_path do
    source 'create-tables.sql'
    owner 'root'
    group 'root'
    mode '0600'
end

# Seed the database with a table and test data.
execute "initialize #{node['awesome_customers_ubuntu']['database']['dbname']} database" do
    command "mysql -h #{node['awesome_customers_ubuntu']['database']['host']} -u #{node['awesome_customers_ubuntu']['database']['admin_username']} -p#{node['awesome_customers_ubuntu']['database']['admin_password']} -D #{node['awesome_customers_ubuntu']['database']['dbname']} < #{create_tables_script_path}"
    not_if  "mysql -h #{node['awesome_customers_ubuntu']['database']['host']} -u #{node['awesome_customers_ubuntu']['database']['admin_username']} -p#{node['awesome_customers_ubuntu']['database']['admin_password']} -D #{node['awesome_customers_ubuntu']['database']['dbname']} -e 'describe customers;'"
end